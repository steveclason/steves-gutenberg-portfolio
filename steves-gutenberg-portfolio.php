<?php
/**
 * Plugin Name: Steve's Portfolio
 * Plugin URI:  https://bitbucket.org/steveclason/steves-gutenberg-portfolio/
 * Description: Custom post-type and Gutenberg blocks for developer's portfolio.
 * Author:      Steve Clason
 * Author URI:  https://steveclason.com
 * Version:     1.0
 * License:     GPL3+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace StevesPortfolio;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * If Gutenberg is not active, display an admin error message
 * and deactivate this plugin.
 */
function require_gutenberg() {
	?>
	<div class="error"><p><?php esc_html_e( 'Steve&apos;s Portfolio requires that the WordPress Gutenberg plugin is activated.', 'steves-gutenberg-portfolio' ); ?></p></div>
	<?php

	deactivate_plugins( plugin_basename( __FILE__ ) );
}

// TODO: When Gutenberg is rolled into WP core, change this to a version_compare() and update the error message.
if ( ! function_exists( 'register_block_type' ) ) {
	add_action( 'admin_notices', __NAMESPACE__ . '\\require_gutenberg' );
	return;
}

/**
 * Initialize plugin.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
